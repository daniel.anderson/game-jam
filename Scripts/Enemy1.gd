extends KinematicBody2D

export (int) var speed = 300
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()
var direction = 1

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	velocity.x = speed * direction
	if direction == 1:
		$AnimatedSprite.flip_h = false
	else:
		$AnimatedSprite.flip_h = true
	$AnimatedSprite.play("walk")
	velocity = move_and_slide(velocity, UP)
	
	if is_on_wall():
		direction = direction * -1
		$RayCast2D.position.x *= -1
		
	if $RayCast2D.is_colliding() == false :
		direction = direction * -1
		$RayCast2D.position.x *= -1
		
	if get_slide_count() > 0:
		for i in range(get_slide_count()):
			if "Zombie" in get_slide_collision(i).collider.name:
				get_slide_collision(i).collider.dead()
	