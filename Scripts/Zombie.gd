extends KinematicBody2D

export (int) var speed = 200
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

var is_dead = false

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("AnimatedSprite")
onready var sprite = self.get_node("AnimatedSprite")

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if is_dead == false:
		if velocity.y != 0:
			animator.play("jump")
		elif velocity.x != 0:
			animator.play("walk")
			if velocity.x > 0:
				sprite.flip_h = false
			else:
				sprite.flip_h = true
		else:
			animator.play("idle")
		if get_slide_count() > 0:
			for i in range(get_slide_count()):
				if "Enemy1" in get_slide_collision(i).collider.name:
					dead()

func dead():
	is_dead = true
	velocity = Vector2(0,0)
	var deadSound = $"Dead/AudioStreamPlayer"
	deadSound.play()
	$AnimatedSprite.play("dead")
	$Timer.start()
	
func _on_Timer_timeout():
	get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
