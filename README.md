The main character is a zombie who wants to go home and rest. But on the way home, he must avoid the zombie-hater humans and spikes they set to "kill" the zombies. 

A 2D Side Scroller Platformer where the player can't attack the enemies. Player only have 1 live, like the popular phrase 'YOLO'.  So avoid the enemies and obstacles carefully because one wrong move and you're 'dead', even though zombies are undead.
This game was made to​ complete 2019/2020 Game Development Course Assignment.

The game itself is just a simple prototype so there will be some bug.

Control :
- Walk right = Right Arrow
- Walk left = Left Arrow
- Jump = Up Arrow 

Asset Credits :
- ​[Super Dialogue Audio Pack](https://jumpcut.itch.io/sdap)
- [​Sounds Effects & Voice lines by PlagueChamp​​](https://twofist.itch.io/plaguechamp-sounds)
- [FREE GRAVEYARD PLATFORMER TILESET​](https://www.gameart2d.com/free-graveyard-platformer-tileset.html)
- [Kenney Game Assets​](https://www.kenney.nl/assets)


[Gitlab Link](https://gitlab.com/daniel.anderson/game-jam)